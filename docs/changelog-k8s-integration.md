# Chagelog

This document represents all the changes made to this repo when integrating k8s in this project.

## Contents
1. [Directory Structure](#directory-structure)
2. [Kubernetes Deployment Scripts](#kubernetes-deployment-scripts)
3. [Why `skaffold`?](#why-skaffold?)
4. [What about secrets?](#what-about-secrets?)
5. [Further improvements](#further-improvements)

### Directory Structure
* Currently, the entire code base runs inside a single container. Seeing that this would lead to complications in the future and very high build times, I broke down this monolith into two microservices.

    1. Frontend
    2. Backend

* As the names suggest, all the frontend related code i.e. ReactJS related code has been sorted into `frontend` folder and all the backend i.e. Django related code has been sorted into the `backend` folder. 

* Just as the folders are now, the main `Dockerfile` has also been split into two dockerfiles, each in `frontend` and `backend` respectively pertaining to their code. This is because we need the dockerfile for each microservice to run on k8s. Note that don't have to run `bootWrapper.sh` in dockerfile because we'll be running the deployment commands separetely in the kubernetes yml files.

### Kubernetes Deployment Scripts
* Introduced a new folder `k8s` which hold kubernetes deployment manifests for `frontend` and `backend`. 

* Each folder contains a `service.yml` and `deploy.yml` which have the manifest of service and deployment.

* Since we need not expose the backend publicly but should be accessible to the frontend, we have applied `.spec.type -> NodePort` for backend and `.spec.type -> LoadBalancer` in the frontend. This way we can access the backend only through the frontend, thus adding an extra layer of security. To test the backend API on production, we can use a tool called `kubefwd` which forwards all kubernetes services to locally, so that we can access the backend using the cluster internal URLs, say `http://crowdalert-backend` will be accessible locally

### Why `skaffold`?

Usual process of deploying to kubernetes:

1. Build the container image
2. Tag the container image
3. Push the image to a container registry
4. Update the deployment manifest with new container image or run the command `kubectl set image deployment/DEPLOYMENT_NAME NEW_IMAGE_NAME`

Deploying with skaffold:

1. `skaffold run` - This command does everything described above which is this less error prone and automates most of the process - The first and foremost reason we wanted to move to k8s is to automate most of the process :)

### What about secrets?
Currently for secrets I haven't made any implementations, because we have multiple options for this. I wanted to communicate with the team once regarding this and finalize which ever is applicable. We can do this in two ways:

#### 1. Using default kuberentes secrets
1. Apply secrets once, do not commit the `secrets.yml` to gitlab and update the file accordingly when new secrets are added.
    ```
    Issues with this implementation

    a. Can't commit these secrets because the secrets will be encoded with base64 which can be decoded
        and converted to human understandable strings (like username and password)

    b. Someone has to take the responsibility of maintaining this and updating this as we can't give 
        access to secrets to everyone
    ```
2. Use gitlab secrets and build a CI pipeline which sets the environment variables in the environment of the yml's defined in the k8s folder and deploy the yml with updated secrets.
    ```
    Issues with this implementation

    This would lead to be a dependency on gitlab, so in future if we want to shift our repo to any other domain like, say github, or if gitlab starts charging customers for the features it'll be difficult for us.
    ```
#### 2. Using vault and consul
    
Since we're already running a kubernetes cluster, we can setup vault and consul containers inside that as well and use vault to store secrets and consul to store key-value pairs. The aim is to run [hashicorp/consul-template](https://hub.docker.com/r/hashicorp/consul-template/) as an `initcontainer` for frontend and backend, and use kubernetes config templates, which define files, without secrets or key value pairs and use them to load data

Example:

`config-templates/local_settings.py.ctmpl` (this file doesn't exist yet but will be created in the root of the project)
```py.ctmpl
# Sample database config with postgres explaining 
# usage of vault secrets and consul key value pairs

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': '{{ key "backend/postgres/NAME" }}',
        {{ with secret "backend/postgres/credentials" -}}
        'USER': '{{ .Data.USERNAME }}',
        'PASSWORD': '{{ .Data.PASSWORD }}',
        {{- end }}
        'HOST': '{{ key "backend/postgres/HOST" }}',
        'PORT': '{{ key "backend/postgres/PORT" }}'
    }
}
```

Here `{{ key "backend/postgres/NAME" }}` will load that particular key from `consul` and the `code snippet` below will load the secret from `vault`
```
{{ with secret "backend/postgres/credentials" -}}
'USER': '{{ .Data.USERNAME }}',
'PASSWORD': '{{ .Data.PASSWORD }}',
{{- end }}
```

This way we can load the secrets and key value pairs before initializing the container and load it to the application directly which decreases the pain of creating `secrets.yml` and not commiting wherein that single person would have to create those secrets all the time and would be a dependency.

Comparing both methods, I think it'd be a good idea to use second method explained, because it doesn't have dependency on anyone and there's no risk of commiting secrets anywhere. Only admins can be given access to vault which in turn gives access to the secrets, and the keys can be stored in consul which doesn't risk even if anyone looks at them as they don't have access to anything, they'll be data like URLs, etc. But even on consul we can enforce access using ACLs.

Kindly note that we do need a way to handle secrets because we can't have secrets in `Dockerfile` as someone might accidentally push their secrets in code.


### Further improvements
* Right now in production, for building the `reactjs` code, we are running a `node` service (i.e. `node build/bundle.js`) which can be optimised. We can build the frontend code (using the command `react-scripts build`) which genrates a `build`/`dist` folder and serve that folder using an `nginx` container. We can also serve `django's static files` from `nginx` as well, i.e. we can have a kubernetes manifest which mounts the nginx location onto it and copies the staticfiles contents to that folder from the folder `staticfiles` in django

    We can separate out the staticfiles generated by frontend code and the django code and serve them both via nginx

* Integrate k8s cluster with gitlab autodevops for seamless deployments

* Use helm to run MongoDB on production (we can use bitnami's chart to deploy with default configuration or as per our requirements). The basic command would be
    ```sh
    helm install crowdalert bitnami/mongodb
    ```
    This will create a `StatefulSet` with the name `crowdalert-mongodb` and mongodb database with GKE PersistentVolume of size `8GB`. We can also specify a `values.yml` file to this command and specify data like `username`, `password`, `persistentVolumeSize`, etc. This will help us in coustomising the `statefulset` we want to create. More info on parameters can be found [here](https://github.com/helm/charts/tree/master/stable/mongodb#parameters)

    We can also have custom deployment ymls for mongodb which can load secrets like username, password from vault as explained earlier [here](#using-vault-and-consul).

* We can have a `Makefile` in our project and define simple commands for deployment in the CI which helps us to track errors(if any) and simplifies code. So for example, for deploying to production, we can run the command `make deploy` and of course create a serviceAccount for gitlab CI and give it only the permissions to do a production deployment, because we can't just let anyone do production deployment! :P 

* We can use the library `standard` for es6 standard formatting for ReactJS (frontend) code and we can use `black` (my personal preference) for Django (backend) code, or other libraries like `autopep8`


# Code Demo

After making all the changes mentioned above, I have deployed the code onto k8s (staging and production). For staging I have used k8s cluster deployed locally using kubernetes on docker (even minikube would do) and for production, I have deployed it on GKE i.e. Google Kubernetes Engine. 

I have attached logs in both the cases. The screenshots look alike, but you can note that on stage, in `service/crowdalert-frontend` the external IP shows `localhost` whereas in production, it shows `35.200.177.33`.

Kindly note that I'm running a preemptive k8s cluster on GKE and `35.200.177.33` might not resolve to a URL by the time someone opens it.

Stage deployment:

![](./images/stage.png)

Production Deployment

![](./images/prod.png)
